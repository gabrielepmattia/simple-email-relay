/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"net/http"
	"relay/api"
	"relay/config"
	"relay/log"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", api.Hello).Methods("GET")
	router.HandleFunc("/send", api.MailSend).Methods("POST")
	router.HandleFunc("/send_test", api.MailSendTest).Methods("GET")

	// cors
	corsSet := cors.New(cors.Options{
		// AllowedOrigins: getAllowedOrigins(),
		// AllowedHeaders: []string{"Authorization", "Content-Type", "Origin",
		//	api.HeaderKeyLimit, api.HeaderKeyOffset, api.HeaderKeyOrder, api.HeaderKeySort,
		//	api.PublicationsQueryHeaderKey, api.PublicationsServiceHeaderKey,
		// "Cache-Control", "Host", "User-Agent", "Accept", "Accept-Encoding", "Connection",
		// },
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "DELETE", "PUT", "OPTIONS"},
		// enable Debugging for testing, consider disabling in production
		Debug: false,
	})

	server := &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%d", config.GetListeningPort()),
		Handler: corsSet.Handler(router),
	}

	log.Log.Infof("Started listening on %d", config.GetListeningPort())
	err := server.ListenAndServe()

	log.Log.Fatalf("Error while starting server: %s", err)
}
