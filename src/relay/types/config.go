/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package types

/*
 * Env
 */

const EnvAllowedAPIKeys = "SMR_ALLOWED_KEYS"
const EnvRunningEnvironment = "SMR_ENV"
const EnvListeningPort = "SMR_PORT"
const EnvHomeCustomHtmlPath = "SMR_HOME_CUSTOM_HTML_PATH"

const EnvSMTPServer = "SMR_SMTP_SERVER"
const EnvSMTPPort = "SMR_SMTP_PORT"
const EnvSMTPUser = "SMR_SMTP_USER"
const EnvSMTPPassword = "SMR_SMTP_PASSWORD"

const EnvFromEmail = "SMR_SMTP_FROM_EMAIL"
const EnvFromName = "SMR_SMTP_FROM_NAME"
const EnvReplayToEmail = "SMR_SMTP_REPLY_TO_EMAIL"

const EnvDKIMEnable = "SMR_DKIM_ENABLE"
const EnvDKIMDomain = "SMR_DKIM_DOMAIN"
const EnvDKIMSelector = "SMR_DKIM_SELECTOR"
const EnvDKIMPrivateKeyPath = "SMR_DKIM_PRIVATE_KEY_PATH"

const EnvTimezone = "TIMEZONE"

/*
 * Running environment
 */

const ConfigRunningEnvironmentProduction = "production"
const ConfigRunningEnvironmentDevelopment = "development"
