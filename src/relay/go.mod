module relay

go 1.19

require (
	github.com/gorilla/mux v1.8.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/rs/cors v1.8.2
	github.com/toorop/go-dkim v0.0.0-20201103131630-e1cd1a0a5208
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)

require (
	github.com/go-test/deep v1.0.8 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
