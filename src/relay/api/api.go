/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package api

import (
	"io"
	"os"
	"relay/config"
	"relay/log"
)

var customHomePageHtml = ""

func init() {
	customHtml := config.GetCustomHomeHtmlPath()
	if customHtml != "" {
		log.Log.Infof("Parsing custom html page")
	}

	homePageFile, err := os.Open(config.GetCustomHomeHtmlPath())
	if err != nil {
		log.Log.Errorf("Cannot open html page: %s", err)
		return
	}

	homePageFileStr, err := io.ReadAll(homePageFile)
	if err != nil {
		log.Log.Errorf("Cannot parse html page: %s", err)
		return
	}

	customHomePageHtml = string(homePageFileStr)
	log.Log.Infof("Custom html parsed successfully")
}
