/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package api

/*
 * Mail
 */

type MailSendRequest struct {
	AddressesTo    []string `json:"addresses_to"`
	AddressesCc    []string `json:"addresses_cc"`
	AddressesCcn   []string `json:"addresses_ccn"`
	AddressReplyTo string   `json:"address_reply_to"`
	Subject        string   `json:"subject"`
	BodyBase64     string   `json:"body_base_64"`
}
