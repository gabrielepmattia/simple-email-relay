/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package api

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"relay/errors"
	"relay/log"
	"relay/mail"
)

func MailSend(w http.ResponseWriter, r *http.Request) {
	if !isKeyAllowed(r) {
		errors.ReplyWithError(&w, errors.NotAuthorized, nil)
		return
	}

	var mailSendRequest MailSendRequest
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Log.Debugf("Cannot read passed configuration payload: %s", err)
		errors.ReplyWithError(&w, errors.InputNotValid, nil)
		return
	}

	err = json.Unmarshal(reqBody, &mailSendRequest)
	if err != nil {
		log.Log.Debugf("Cannot unmarshal passed configuration payload: %s", err)
		errors.ReplyWithError(&w, errors.MarshalError, nil)
		return
	}

	// addresses to are necessary
	if len(mailSendRequest.AddressesTo) == 0 {
		log.Log.Debugf("Addresses are empty: %s", err)
		errors.ReplyWithError(&w, errors.InputNotValid, nil)
		return
	}

	// convert body from base64
	var body []byte

	if mailSendRequest.BodyBase64 != "" {
		body, err = base64.StdEncoding.DecodeString(mailSendRequest.BodyBase64)
		if err != nil {
			log.Log.Debugf("Cannot base64 decode: %s", err)
			errors.ReplyWithError(&w, errors.InputNotValid, nil)
			return
		}
	}

	err = mail.SendEmail(mailSendRequest.AddressesTo, mailSendRequest.AddressReplyTo, mailSendRequest.Subject, string(body))
	if err != nil {
		log.Log.Errorf("[%s] could not send email: %s", r.RemoteAddr, err)
		errors.ReplyWithErrorMessage(&w, errors.MailSendGenericError, err.Error(), nil)
		return
	}

	log.Log.Infof("[%s] mail successfully sent", r.RemoteAddr)
	w.WriteHeader(200)
}

func MailSendTest(w http.ResponseWriter, r *http.Request) {
	if !isKeyAllowed(r) {
		errors.ReplyWithError(&w, errors.NotAuthorized, nil)
		return
	}

	addressTo := parseQueryString(r, "to", "")
	if addressTo == "" {
		errors.ReplyWithError(&w, errors.InputNotValid, nil)
		return
	}

	err := mail.SendTestEmail(addressTo)
	if err != nil {
		log.Log.Errorf("[%s] could not send email: %s", r.RemoteAddr, err)
		errors.ReplyWithErrorMessage(&w, errors.MailSendGenericError, err.Error(), nil)
		return
	}

	w.WriteHeader(200)
}
