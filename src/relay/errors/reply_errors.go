/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package errors

import (
	"encoding/json"
	"net/http"
	"relay/utils"
)

type ErrorReply struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Err     error  `json:"err,omitempty"`
}

const (
	GenericError         int = 1
	GenericNotFoundError int = 2
	InputNotValid        int = 3
	MarshalError         int = 4
	NotAuthorized        int = 5

	MailSendGenericError int = 100
)

var errorMessages = map[int]string{
	1: "Generic Error",
	2: "Not Found",
	3: "Passed input is not correct or malformed",
	4: "Cannot marshal the struct",
	5: "API Key missing or not valid",

	100: "Could not send the email",
}

var errorStatus = map[int]int{
	1: 500,
	2: 404,
	3: 400,
	4: 400,
	5: 403,

	100: 500,
}

func GetErrorJson(errorCode int) (int, string, error) {
	var errorReply = ErrorReply{Code: errorCode, Message: errorMessages[errorCode]}
	errorReplyJSON, err := json.Marshal(errorReply)

	if err != nil {
		return -1, "", err
	}

	return errorStatus[errorCode], string(errorReplyJSON), nil
}

func GetErrorJsonMessage(errorCode int, msg string) (int, string, error) {
	var errorReply = ErrorReply{Code: errorCode, Message: msg}
	errorReplyJSON, err := json.Marshal(errorReply)

	if err != nil {
		return -1, "", err
	}

	return errorStatus[errorCode], string(errorReplyJSON), nil
}

func ReplyWithError(w *http.ResponseWriter, errorCode int, customHeaders *map[string]string) {
	errorStatusCode, errorResponseJson, _ := GetErrorJson(errorCode)

	utils.HttpSendJSONResponse(w, errorStatusCode, errorResponseJson, customHeaders)
}

func ReplyWithErrorMessage(w *http.ResponseWriter, errorCode int, msg string, customHeaders *map[string]string) {
	var errorReply = ErrorReply{Code: errorCode, Message: msg}
	errorReplyJSON, _ := json.Marshal(errorReply)

	utils.HttpSendJSONResponse(w, errorStatus[errorCode], string(errorReplyJSON), customHeaders)
}
