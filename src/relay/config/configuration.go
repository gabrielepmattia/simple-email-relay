/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"os"
	"relay/types"
	"strconv"
	"strings"
)

/*
 * Getters
 */

func GetAllowedApiKeys() []string {
	return configuration.allowedApiKeys
}
func GetListeningPort() uint {
	return configuration.listeningPort
}
func GetRunningEnvironment() string {
	return configuration.runningEnvironment
}
func GetSMTPServer() string {
	return configuration.smtpServer
}
func GetSMTPUser() string {
	return configuration.smtpUser
}
func GetSMTPPassword() string {
	return configuration.smtpPassword
}
func GetSMTPPort() int {
	return configuration.smtpPort
}
func GetSMTPFromEmail() string {
	return configuration.smtpFromEmail
}
func GetSMTPFromName() string {
	return configuration.smtpFromName
}
func GetSMTPReplyToEmail() string {
	return configuration.smtpReplyToEmail
}
func GetTimezone() string {
	return configuration.timezone
}
func IsProduction() bool {
	return configuration.runningEnvironment != types.ConfigRunningEnvironmentDevelopment
}
func GetDKIMEnabled() bool {
	return configuration.dkimEnable
}
func GetDKIMDomain() string {
	return configuration.dkimDomain
}
func GetDKIMSelector() string {
	return configuration.dkimSelector
}
func GetDKIMPrivateKeyPath() string {
	return configuration.dkimPrivateKeyPath
}
func GetCustomHomeHtmlPath() string {
	return configuration.homeCustomHtmlPath
}

/*
 * Utils
 */

func readConfigEnv() {
	configuration.timezone = parseEnvStrConfiguration(types.EnvTimezone, DefaultTimeZone)
	configuration.runningEnvironment = parseEnvStrConfiguration(types.EnvRunningEnvironment, DefaultRunningEnvironment)
	configuration.smtpServer = parseEnvStrConfiguration(types.EnvSMTPServer, DefaultSMTPServer)
	configuration.smtpUser = parseEnvStrConfiguration(types.EnvSMTPUser, DefaultSMTPUser)
	configuration.smtpPassword = parseEnvStrConfiguration(types.EnvSMTPPassword, DefaultSMTPPassword)
	configuration.smtpFromEmail = parseEnvStrConfiguration(types.EnvFromEmail, DefaultSMTPFromEmail)
	configuration.smtpFromName = parseEnvStrConfiguration(types.EnvFromName, DefaultSMTPFromName)
	configuration.smtpReplyToEmail = parseEnvStrConfiguration(types.EnvReplayToEmail, DefaultSMTPReplyToEmail)
	configuration.homeCustomHtmlPath = parseEnvStrConfiguration(types.EnvHomeCustomHtmlPath, DefaultCustomHomeHtml)

	configuration.dkimEnable = parseEnvBoolConfiguration(types.EnvDKIMEnable, DefaultDKIMEnabled)
	configuration.dkimDomain = parseEnvStrConfiguration(types.EnvDKIMDomain, DefaultDKIMDomain)
	configuration.dkimSelector = parseEnvStrConfiguration(types.EnvDKIMSelector, DefaultDKIMSelector)
	configuration.dkimPrivateKeyPath = parseEnvStrConfiguration(types.EnvDKIMPrivateKeyPath, DefaultDKIMPrivateKeyPath)

	// numbers
	configuration.listeningPort = uint(parseEnvIntConfiguration(types.EnvListeningPort, DefaultListeningPort))
	configuration.smtpPort = parseEnvIntConfiguration(types.EnvSMTPPort, DefaultSMTPPort)
	// arrays
	configuration.allowedApiKeys = parseEnvStrArrConfiguration(types.EnvAllowedAPIKeys, []string{})

	// update fields
	if configuration.runningEnvironment != types.ConfigRunningEnvironmentDevelopment {
		configuration.runningEnvironment = types.ConfigRunningEnvironmentProduction
	}
}

func parseEnvStrConfiguration(fieldKey string, defaultValue string) string {
	fieldValue := os.Getenv(fieldKey)
	if fieldValue == "" {
		return defaultValue
	}
	return fieldValue
}

func parseEnvBoolConfiguration(fieldKey string, defaultValue bool) bool {
	fieldValue := parseEnvStrConfiguration(fieldKey, "")
	if fieldValue == "" {
		return defaultValue
	}

	if strings.ToLower(fieldValue) == "true" {
		return true
	}

	if strings.ToLower(fieldValue) == "false" {
		return false
	}

	return defaultValue
}

func parseEnvStrArrConfiguration(fieldKey string, defaultValue []string) []string {
	fieldValue := parseEnvStrConfiguration(fieldKey, "")
	if fieldValue == "" {
		return defaultValue
	}

	// split
	return strings.Split(fieldValue, ",")
}

func parseEnvIntConfiguration(fieldKey string, defaultValue int) int {
	fieldValue, err := strconv.Atoi(os.Getenv(fieldKey))
	if err != nil {
		return defaultValue
	}
	return fieldValue
}
