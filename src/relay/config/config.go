/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"relay/log"
	"relay/types"
)

const AppName = "simple-email-relay"
const AppVersion = "0.0.1b"
const AppVersionCommit = "xxx"

/*
 * Default Parameters
 */

const DefaultListeningPort = 80
const DefaultTimeZone = "Europe/Rome"
const DefaultRunningEnvironment = types.ConfigRunningEnvironmentProduction

/*
 * SMTP
 */

const DefaultSMTPServer = ""
const DefaultSMTPPort = 587
const DefaultSMTPUser = ""
const DefaultSMTPPassword = ""
const DefaultSMTPFromEmail = "smr-noreply@localhost"
const DefaultSMTPFromName = "Simple eMail Relay"
const DefaultSMTPReplyToEmail = "smr-noreply@localhost"
const DefaultCustomHomeHtml = ""

const DefaultDKIMEnabled = false
const DefaultDKIMDomain = ""
const DefaultDKIMSelector = ""
const DefaultDKIMPrivateKeyPath = ""

var configuration ConfigurationSet

func init() {
	// init base configuration
	readConfigEnv()

	log.Log.Info("Starting in %s environment", GetRunningEnvironment())
}
