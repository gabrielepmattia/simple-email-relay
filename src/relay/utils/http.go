/*
 * Simple eMail Relay
 * Copyright (c) 2022. Gabriele Proietti Mattia <pm.gabriele@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package utils

import (
	"io"
	"net/http"
	"relay/log"
)

/*
* Utils
 */

func HttpSendJSONResponse(w *http.ResponseWriter, code int, body string, customHeaders *map[string]string) {
	(*w).Header().Set("Content-Type", "application/json")
	HttpAddHeadersToResponse(w, customHeaders)

	(*w).WriteHeader(code)

	_, err := io.WriteString(*w, body)
	if err != nil {
		log.Log.Debugf("Cannot send response: %s", err.Error())
	}
}

func HttpSendJSONResponseByte(w *http.ResponseWriter, code int, body []byte, customHeaders *map[string]string) {
	(*w).Header().Set("Content-Type", "application/json")
	HttpAddHeadersToResponse(w, customHeaders)

	(*w).WriteHeader(code)

	_, err := (*w).Write(body)
	if err != nil {
		log.Log.Debugf("Cannot send response: %s", err.Error())
	}
}

func HttpAddHeadersToResponse(w *http.ResponseWriter, customHeaders *map[string]string) {
	if customHeaders == nil {
		return
	}

	for key, value := range *customHeaders {
		(*w).Header().Set(key, value)
	}
}

func HttpParseXHeaders(headers http.Header) *map[string]string {
	outputHeaders := map[string]string{}

	for key, value := range headers {
		if string(key[0]) == "X" {
			outputHeaders[key] = value[0]
		}
	}

	return &outputHeaders
}

